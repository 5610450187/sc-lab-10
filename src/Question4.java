import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;




public class Question4 implements ActionListener{

	private JFrame frame;
	private JPanel panel;
	private JPanel control;
	
	private JComboBox button;


	public static void main(String[] args) {
		new Question4();
	}

	public Question4(){
		frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(400, 300);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		FlowLayout flowlyt = new FlowLayout();	
		panel = new JPanel();
		panel.setLayout(flowlyt);


//		flowlyt.setAlignment(FlowLayout.TRAILING);
//		control = new JPanel();
//		control.setLayout(new FlowLayout());

		createButton();
		button.addActionListener(this);

		frame.getContentPane().add(panel);
//		frame.getContentPane().add(control,BorderLayout.SOUTH);
		
	}
	
	private void createButton(){
		button = new JComboBox();
		button.addItem(" RED ");
		button.addItem(" GREEN ");
		button.addItem(" BLUE ");
		
		panel.add(button);
	
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		
		if(" RED ".equals(button.getSelectedItem())){
			 panel.setBackground(new Color(255,0,0));
			 		
		}
		
		else if(" GREEN ".equals(button.getSelectedItem())){
			 panel.setBackground(new Color(0,255,0));
			 			 
		}
		else if(" BLUE ".equals(button.getSelectedItem())){
			 panel.setBackground(new Color(0,0,255));
			
		}
		
	}
		
	


}
