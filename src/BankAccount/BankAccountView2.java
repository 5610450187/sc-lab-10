package BankAccount;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class BankAccountView2 implements ActionListener {

	private JFrame frame;
	private JPanel panel;
	private JLabel mlabel;
	private JTextField  txt;
	private JTextArea resultArea;
	private JButton depButton,withButton;
	private BankAccount account;

	public static void main(String[] args) {
		new BankAccountView2();

	}

	public BankAccountView2() {
		frame = new JFrame();
		account = new BankAccount();

		createText();
		createButton();
		createPanel();

		depButton.addActionListener(this);
		withButton.addActionListener(this);
		frame.setVisible(true);
		frame.setSize(400, 250);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void createText(){
		mlabel = new JLabel("money :");
		resultArea = new JTextArea(10,30);
		resultArea.setEditable(false);
		txt = new JTextField(8);
	}

	private void createButton(){
		depButton = new JButton("deposit");
		withButton = new JButton("withdraw");

	}

	private void createPanel(){
		panel = new JPanel();
		

		panel.add(mlabel);
		panel.add(txt);
		panel.add(depButton);
		panel.add(withButton);
		panel.add(resultArea);
		
		JScrollPane scrollPane = new JScrollPane(resultArea);
		
		panel.add(scrollPane);
		
		frame.add(panel);
		
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if("deposit".equals(str)){
			double amount = Double.parseDouble(txt.getText());
			account.deposit(amount);
			resultArea.append(account.getBalance()+"\n");

		}
		else if("withdraw".equals(str)){
			double amount = Double.parseDouble(txt.getText());
			account.withdraw(amount);
			resultArea.append(account.getBalance()+"\n");

		}


	}



}
