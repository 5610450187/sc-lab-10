package BankAccount;

import java.awt.BorderLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BankAccountView1 implements ActionListener {

	private JFrame frame;
	private JPanel panel,buttonPanel;
	private JLabel mlabel,blabel;
	private JTextField  txt;
	private JButton depButton,withButton;
	private BankAccount account;

	public static void main(String[] args) {
		new BankAccountView1();

	}

	public BankAccountView1() {
		frame = new JFrame();
		account = new BankAccount();

		createText();
		createButton();
		createPanel();

		depButton.addActionListener(this);
		withButton.addActionListener(this);
		frame.setVisible(true);
		frame.setSize(500, 100);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	private void createText(){
		mlabel = new JLabel("money :");
		blabel = new JLabel("balance: " + account.getBalance());
		txt = new JTextField(8);
	}

	private void createButton(){
		depButton = new JButton("deposit");
		withButton = new JButton("withdraw");

	}

	private void createPanel(){
		panel = new JPanel();
		buttonPanel = new JPanel();

		panel.add(mlabel);
		panel.add(txt);
		panel.add(blabel);
		
		
		buttonPanel.add(depButton);
		buttonPanel.add(withButton);
		frame.add(panel);
		frame.getContentPane().add(buttonPanel,BorderLayout.SOUTH);
	}



	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if("deposit".equals(str)){
			double amount = Double.parseDouble(txt.getText());
			account.deposit(amount);
			blabel.setText("balance: " + account.getBalance());

		}
		else if("withdraw".equals(str)){
			double amount = Double.parseDouble(txt.getText());
			account.withdraw(amount);
			blabel.setText("balance: " + account.getBalance());

		}


	}



}
