import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JRadioButton;



public class Question2 implements ActionListener{

	private JFrame frame;
	private JPanel panel;
	private JPanel control;
	
	private JRadioButton buttonR;
	private JRadioButton buttonG;
	private JRadioButton buttonB;

	
	public static void main(String[] args) {
		new Question2();
	}

	public Question2(){
		frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(400, 300);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		FlowLayout flowlyt = new FlowLayout();	
		panel = new JPanel();
		panel.setLayout(flowlyt);


		flowlyt.setAlignment(FlowLayout.TRAILING);
		control = new JPanel();
		control.setLayout(new FlowLayout());

		createButton();
		buttonR.addActionListener(this);
		buttonG.addActionListener(this);
		buttonB.addActionListener(this);
		frame.getContentPane().add(panel);
		frame.getContentPane().add(control,BorderLayout.SOUTH);
		
	}
	
	private void createButton(){
		buttonR = new JRadioButton(" RED ");
		buttonG = new JRadioButton(" GREEN ");
		buttonB = new JRadioButton(" BLUE ");
		
		ButtonGroup group = new ButtonGroup(); 
		group.add(buttonR);
		group.add(buttonG);
		group.add(buttonB);

		control.add(buttonR);
		control.add(buttonG);
		control.add(buttonB);		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if(" RED ".equals(str)){
			 panel.setBackground(new Color(255,0,0));
			 		
		}
		else if(" GREEN ".equals(str)){
			 panel.setBackground(new Color(0,255,0));
			 			 
		}
		else if(" BLUE ".equals(str)){
			 panel.setBackground(new Color(0,0,255));
			
		}
		
	}
		
	


}
