import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class Question3 implements ActionListener{

	private JFrame frame;
	private JPanel panel;
	private JPanel control;
	private JCheckBox buttonR;
	private JCheckBox buttonG;
	private JCheckBox buttonB;

	
	public static void main(String[] args) {
		new Question3();
	}

	public Question3(){
		frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(400, 300);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		FlowLayout flowlyt = new FlowLayout();	
		panel = new JPanel();
		panel.setLayout(flowlyt);


		flowlyt.setAlignment(FlowLayout.TRAILING);
		control = new JPanel();
		control.setLayout(new FlowLayout());

		createButton();
		buttonR.addActionListener(this);
		buttonG.addActionListener(this);
		buttonB.addActionListener(this);
		frame.getContentPane().add(panel);
		frame.getContentPane().add(control,BorderLayout.SOUTH);
		
	}
	
	private void createButton(){
		buttonR = new JCheckBox(" RED ");	
		buttonG = new JCheckBox(" GREEN ");
		buttonB = new JCheckBox(" BLUE ");

		control.add(buttonR);
		control.add(buttonG);
		control.add(buttonB);		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		int r = 0;
		int g = 0;
		int b = 0;
		if(" RED ".equals(str) && buttonR.isSelected()){
			r = 255;
		}else{
			r = 0;
		}
		if(" GREEN ".equals(str)&& buttonG.isSelected()){
			g = 255;	 
		}else{
			g = 0;
		}
		if(" BLUE ".equals(str)&& buttonB.isSelected()){
			b = 255;
	
		}else{
			b = 0;
		}
		panel.setBackground(new Color(r,g,b));
		
		
	}
		
	


}
