import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;



public class Question1 implements ActionListener{

	private JFrame frame;
	private JPanel panel;
	private JPanel control;

	private JButton buttonR;
	private JButton buttonG;
	private JButton buttonB;

	
	public static void main(String[] args) {
		new Question1();
	}

	public Question1(){
		frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(400, 300);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		FlowLayout flowlyt = new FlowLayout();	
		panel = new JPanel();
		panel.setLayout(flowlyt);


		flowlyt.setAlignment(FlowLayout.TRAILING);
		control = new JPanel();
		control.setLayout(new FlowLayout());

		createButton();
		buttonR.addActionListener(this);
		buttonG.addActionListener(this);
		buttonB.addActionListener(this);
		frame.getContentPane().add(panel);
		frame.getContentPane().add(control,BorderLayout.SOUTH);
		
	}
	
	private void createButton(){
		buttonR = new JButton(" RED ");	
		buttonG = new JButton(" GREEN ");
		buttonB = new JButton(" BLUE ");

		control.add(buttonR);
		control.add(buttonG);
		control.add(buttonB);		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if(" RED ".equals(str)){
			 panel.setBackground(new Color(255,0,0));
			 		
		}
		else if(" GREEN ".equals(str)){
			 panel.setBackground(new Color(0,255,0));
			 			 
		}
		else if(" BLUE ".equals(str)){
			 panel.setBackground(new Color(0,0,255));
			
		}
		
	}
		
	


}
