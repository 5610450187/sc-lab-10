
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;



public class Question5 implements ActionListener{

	private JFrame frame;
	private JPanel panel;
	private JMenuBar menubar;
	private JMenu menu;
	private JMenuItem menuR,menuG,menuB;

	
	public static void main(String[] args) {
		new Question5();
	}

	public Question5(){
		frame = new JFrame();

		frame.setVisible(true);
		frame.setSize(400, 300);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


		FlowLayout flowlyt = new FlowLayout();	
		panel = new JPanel();
		panel.setLayout(flowlyt);



		createButton();
		frame.setJMenuBar(menubar);
		menuR.addActionListener(this);
		menuG.addActionListener(this);
		menuB.addActionListener(this);

		frame.getContentPane().add(panel);
		
	}
	
	private void createButton(){
		menubar = new JMenuBar();
		menu = new JMenu("Color");
		menuR = new JMenuItem(" RED ");
		menuG = new JMenuItem(" GREEN ");
		menuB = new JMenuItem(" BLUE ");
		menubar.add(menu);
		menu.add(menuR);
		menu.add(menuG);
		menu.add(menuB);
		
		
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		String str = e.getActionCommand();
		if(" RED ".equals(str)){
			 panel.setBackground(new Color(255,0,0));
			 		
		}
		else if(" GREEN ".equals(str)){
			 panel.setBackground(new Color(0,255,0));
			 			 
		}
		else if(" BLUE ".equals(str)){
			 panel.setBackground(new Color(0,0,255));
			
		}
		
	}
		
	


}
